#!/bin/sh

VERSION="0.5.1"

if [ -n "${XDG_CACHE_HOME}" ]; then
	_PREFIX="${XDG_CACHE_HOME}/"
else
	_PREFIX="${HOME}/."
fi
PREFIX="${CM_DIR:-${_PREFIX}cm}"
MODE="${CM_CLIPBOARD:-primary}"
ENTRIES="${CM_ENTRIES:-10}"
SIZE="${CM_MIN_ENTRY_SIZE:-8}"
AUTOSORT="${CM_AUTOSORT:-false}"

_deploy_or_cleanup() {
	if [ -e "${PREFIX}" ] && [ ! -d "${PREFIX}" ]; then
		printf "“%s” exists, but it is not a directory.\n" "${PREFIX}" 1>&2
		exit 1
	elif [ ! -e "${PREFIX}" ]; then
		mkdir -p "${PREFIX}"
	else
		for file in $(ls -1t "${PREFIX}/" | tail -n+$((${ENTRIES}+1))); do
			rm -f "${PREFIX}/${file}" 2>/dev/null
		done
	fi
}

trap _deploy_or_cleanup INT TERM EXIT

_check_entry_size() {
	[ ${SIZE} -eq -1 ] && return 0
	local strlen=$({ printf '%s' "${@}" | wc -c; } 2>/dev/null)
	if [ ${strlen} -ge ${SIZE} ]; then
		printf '%s' "${@}"
	fi
}

_check_action() {
	local num=$(printf '%d' ${1} 2>/dev/null)
	local count=$({ ls -1 "${PREFIX}/" | wc -l; } 2>/dev/null)
	if [ -z "${num}" ] || [ ${num} -gt ${ENTRIES} ] || [ ${num} -gt ${count} ]; then
		printf '“%d” is invalid entry.\n' ${num} 1>&2
		exit 1
	fi
	local i=0
	for file in $(ls -1t "${PREFIX}/"); do
		i=$((i + 1))
		if [ ${i} -eq ${num} ]; then
			printf '%s' "${PREFIX}/${file}"
			return 0
		fi
	done
	return 1
}

cm_pull() {
	local file="$(_check_action ${1})"
	if [ -n "${file}" ]; then
		xclip -selection "${MODE}" -i "${file}" 2>/dev/null
		[ "${AUTOSORT}" = "true" ] && touch "${file}" 2>/dev/null || true
	fi
}

cm_rm() {
	local file="$(_check_action ${1})"
	[ -n "${file}" ] && rm "${file}" 2>/dev/null
}

cm_push() {
	local clip="$(xclip -selection "${MODE}" -o 2>/dev/null)"
	if [ ! -z "$(_check_entry_size "${clip}")" ]; then
		local md5="$({ printf '%s' "${clip}" | md5sum | cut -d' ' -f1; } 2>/dev/null)"
		{ printf '%s' "${clip}" | tee >"${PREFIX}/${md5}" | xclip -selection "${MODE}" -i; } 2>/dev/null
	else
		printf 'Clip is too small.' 1>&2
		exit 1
	fi
}

cm_ls() {
	local i=0
	for file in $(ls -1t "${PREFIX}/" 2>/dev/null); do
		i=$((${i}+1))
		local content="$({ grep -v "^$" "${PREFIX}/${file}" | head -n1 | sed "s#^\s*##"; } 2>/dev/null)"
		{ printf '%d\t%s\n' "${i}" "$(printf '%s' "${content}" | cut -c-72)" | expand -t4; } 2>/dev/null
	done
}

cm_in() {
	if [ -z "${@}" ]; then
		printf 'STDIN is empty.\n' 1>&2
		exit 1
	fi
	if [ ! -z "$(_check_entry_size "${@}")" ]; then
		local md5="$(printf '%s' "${@}" | md5sum | cut -f1 -d' ')"
		printf '%s' "${@}" | tee "${PREFIX}/${md5}" | xclip -selection "${MODE}" -i
	else
		printf 'Input is too small.\n' 1>&2
		return 1
	fi
}

cm_switch() {
	local ALT_MODE=""
	if [ "${MODE}" = "primary" ]; then
		ALT_MODE="clipboard"
	elif [ "${MODE}" = "clipboard" ]; then
		ALT_MODE="primary"
	else
		return 1
	fi
	local mode="$(xclip -selection "${MODE}" -o 2>/dev/null)"
	local alt_mode="$(xclip -selection "${ALT_MODE}" -o 2>/dev/null)"
	{ printf '%s' "${mode}" | xclip -selection "${ALT_MODE}" -i; } 2>/dev/null
	{ printf '%s' "${alt_mode}" | xclip -selection "${MODE}" -i; } 2>/dev/null
}

cm_help() {
	cat 1>&2 <<-EOF
		${0##*/} ${VERSION}

		Usage: ${0##*/} <OPTION> [ARGUMENT]
		  print        Print clipboard content.
		  push         Save clipboard to file.
		  pull <N>     Copy saved clipboard to clipboard.
		  rm <N>       Delete saved clipboard.
		  ls           List all saved clipboards.
		  in [STRING]  Save STDIN (or pipe) to file.
		  clear        Clear clipboard.
		  switch       Switch clipboards (primary [Shift+Insert] <-> clipboard [Control+V]).
		  purge        Delete all saved clipboards.
	EOF
}

_deploy_or_cleanup

case ${1} in
	print) xclip -selection "${MODE}" -o 2>/dev/null ;;
	push) cm_push ;;
	pull) shift; cm_pull ${1} ;;
	rm) shift; cm_rm "${1}" ;;
	ls|'') cm_ls ;;
	in) shift; if [ ${#} -eq 0 ]; then
			set -- -
			cm_in "$(cat "${@}" 2>/dev/null)"
		else
			cm_in "$(printf '%s' "${@}" 2>/dev/null)"
		fi ;;
	show) xclip -selection "${MODE}" -o 2>/dev/null ;;
	clear) { printf '' | xclip -selection "${MODE}" -i; } 2>/dev/null ;;
	switch) cm_switch ;;
	purge) [ -d "${PREFIX}" ] && rm -f "${PREFIX}"/* 2>/dev/null ;;
	-h|--help|help) cm_help ;;
	*) printf 'Invalid argument "%s"\n' "${1}" 1>&2; cm_help ;;
esac
