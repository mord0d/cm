cm
==

**cm** is a Clipboard Manager, written in Shell.

## Dependencies

- cm
  - coreutils
  - grep
  - sed
  - xclip

- cm-rofi
  - coreutils
  - grep
  - sed
  - rofi
